#!/bin/sh
# autogen.sh
#
# Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
# 
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see
#  <http://www.gnu.org/licenses/>.

(echo "@setfilename libtarot.info" \
     && echo '\bye') > doc/libtarot.texi || exit 1
touch AUTHORS || exit 1
touch README || exit 1
gnulib-tool --libtool \
	    --import assert stdalign xalloc \
	    gitlog-to-changelog git-version-gen || exit 1
./gitlog-to-changelog > ChangeLog
touch README || exit 1
autoreconf -vif || exit 1
rm doc/libtarot.texi || exit 1
rm AUTHORS || exit 1
rm README || exit 1
