<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:tarot="http://planete-kraus.eu/tarot"
		version="1.0">

  <xsl:template match="tarot:example">
    <html>
      <body>
	<xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="tarot:test">
    <h1><xsl:value-of select="@description" /></h1>
    <svg width="{@width}" height="{@height}"
	 viewbox="{-(@width div 2)} {-(@height div 2)} {@width} {@height}">
      <xsl:apply-templates select="tarot:card">
	<xsl:sort select="@z" />
      </xsl:apply-templates>
    </svg>
  </xsl:template>

  <xsl:template match="tarot:card">
    <g transform="translate({@x}, {-@y}) scale({@scale}) rotate({-@angle * 180 div 3.141592})">
      <xsl:comment>z = <xsl:value-of select="@z" />, index = <xsl:value-of select="@index" /></xsl:comment>
      <rect width="{../@card-width}" height="{../@card-height}"
	    x="{- ../@card-width div 2}" y="{- ../@card-height div 2}"
	    stroke="black" fill="yellow"/>
    </g>
  </xsl:template>
</xsl:stylesheet>
