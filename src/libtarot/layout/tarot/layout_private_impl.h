/**
 * file tarot/layout_private_impl.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_LAYOUT_PRIVATE_IMPL_INCLUDED
#define H_TAROT_LAYOUT_PRIVATE_IMPL_INCLUDED

#include <tarot/layout_private.h>

static inline void
skip_adversaries (size_t n_adversaries, const size_t *n_adv_cards,
                  size_t max_dst, TarotPosition * dst, size_t *start)
{
  size_t i;
  for (i = 0; i < n_adversaries; i++)
    {
      size_t j;
      for (j = 0; j < n_adv_cards[i]; j++)
        {
          if (*start < max_dst)
            {
              dst[*start].x = 0;
              dst[*start].y = 0;
              dst[*start].z = 0;
              dst[*start].scale = 0;
              dst[*start].angle = 0;
            }
          *start = *start + 1;
        }
    }
}

static inline size_t
layout_position (double view_w, double view_h, double card_w, double card_h,
                 size_t n_my_cards, size_t n_adversaries,
                 const size_t *n_adv_cards, size_t n_center, size_t max_dst,
                 TarotPosition * dst)
{
  /* First, the hand, then skip the adversaries, then the trick, then
     the center. */
  size_t ret = 0;
  if (n_center != 0)
    {
      /* 1/3, 1/3, 1/3 */
      size_t i;
      layout_hand (view_w, view_h / 3, card_w, card_h, n_my_cards,
                   (ret < max_dst ? max_dst - ret : 0), 0, dst + ret);
      for (i = 0; i < n_my_cards; i++)
        {
          if (ret < max_dst)
            {
              dst[ret].y -= view_h / 3.0;
              dst[ret].z += 1;
              dst[ret].z /= 3.0;
            }
          ret++;
        }
      /* Skip the adversaries */
      skip_adversaries (n_adversaries, n_adv_cards, max_dst, dst, &ret);
      layout_trick (view_w, view_h / 3, card_w, card_h, n_adversaries + 1,
                    (ret < max_dst ? max_dst - ret : 0), 0, dst + ret);
      for (i = 0; i < n_adversaries + 1; i++)
        {
          if (ret < max_dst)
            {
              dst[ret].y += view_h / 3.0;
              dst[ret].z += 0;
              dst[ret].z /= 3.0;
            }
          ret++;
        }
      layout_center (view_w, view_h / 3, card_w, card_h, n_center,
                     (ret < max_dst ? max_dst - ret : 0), 0, dst + ret);
      for (i = 0; i < n_center; i++)
        {
          if (ret < max_dst)
            {
              dst[ret].z += 2;
              dst[ret].z /= 3.0;
            }
          ret++;
        }
    }
  else
    {
      /* 1/2, 1/2 */
      size_t i;
      layout_hand (view_w, view_h / 2, card_w, card_h, n_my_cards,
                   (ret < max_dst ? max_dst - ret : 0), 0, dst + ret);
      for (i = 0; i < n_my_cards; i++)
        {
          if (ret < max_dst)
            {
              dst[ret].y -= view_h / 4.0;
              dst[ret].z += 1;
              dst[ret].z /= 2.0;
            }
          ret++;
        }
      /* Skip the adversaries */
      skip_adversaries (n_adversaries, n_adv_cards, max_dst, dst, &ret);
      layout_trick (view_w, view_h / 2, card_w, card_h, n_adversaries + 1,
                    (ret < max_dst ? max_dst - ret : 0), 0, dst + ret);
      for (i = 0; i < n_adversaries + 1; i++)
        {
          if (ret < max_dst)
            {
              dst[ret].y += view_h / 4.0;
              dst[ret].z += 0;
              dst[ret].z /= 2.0;
            }
          ret++;
        }
    }
  return ret;
}

static inline size_t
layout_hand_one_row (double view_w, double view_h, double card_w,
                     double card_h, size_t n_cards, size_t max, size_t start,
                     TarotPosition * position)
{
  const double theta_max = M_PI / 6;
  const double r_min = view_h / (2 * (1 - cos (theta_max)));
  const double radius_naive =
    (view_w * view_w + view_h * view_h) / (4 * view_h);
  const double radius = (radius_naive < r_min ? r_min : radius_naive);
  const double theta = asin (view_w / (2 * radius));
  const double card_scale = view_h / card_h;
  const double scaled_card_w = card_scale * card_w;
  const double alpha = asin (scaled_card_w / (2 * radius));
  const double radius_center = radius;
  const double center_y = -radius_center;
  const double n = (double) n_cards + 1;
  size_t i = 0;
  for (i = 0; i < max && i + start < n_cards; i++)
    {
      const double adjusted_radius = radius - 0.05 * view_h;
      const double i_f = (double) (i + start) + 1;
      const double pos = i_f / n;
      const double position_sym = 2 * pos - 1;
      const double angle = position_sym * (theta - alpha);
      const double x = adjusted_radius * sin (angle);
      const double y = center_y + adjusted_radius * cos (angle);
      const double z = pos / 2;
      position[i].x = x;
      position[i].y = y;
      position[i].z = z;
      position[i].scale = card_scale;
      position[i].angle = -angle;
    }
  return n_cards;
}

static inline size_t
layout_hand_two_rows (double view_w, double view_h, double card_w,
                      double card_h, size_t n_cards, size_t max, size_t start,
                      TarotPosition * position)
{
  size_t n_last = n_cards / 2;
  size_t n_first = n_cards - n_last;
  size_t i;
  size_t n_written = n_first - start;
  if (start > n_first)
    {
      n_written = 0;
    }
  layout_hand_one_row (view_w, view_h / 2, card_w, card_h,
                       n_first, max, start, position);
  for (i = 0; i < max && i + start < n_first; i++)
    {
      position[i].y += view_h / 4;
      position[i].z /= 2;
    }
  if (n_written < max)
    {
      max -= n_written;
    }
  else
    {
      max = 0;
    }
  position += n_written;
  start = 0;
  layout_hand_one_row (view_w, view_h / 2, card_w, card_h,
                       n_last, max, start, position);
  for (i = 0; i < max && i < n_last; i++)
    {
      position[i].y -= view_h / 4;
      position[i].z += 1;
      position[i].z /= 2;
    }
  return n_cards;
}

static inline size_t
layout_hand (double view_w, double view_h, double card_w, double card_h,
             size_t n_cards, size_t max, size_t start,
             TarotPosition * position)
{
  double card_w_resized = card_w * view_h / card_h;
  size_t n_cards_max = 2 * view_w / card_w_resized;
  if (n_cards_max >= n_cards)
    {
      return layout_hand_one_row (view_w, view_h, card_w, card_h,
                                  n_cards, max, start, position);
    }
  return layout_hand_two_rows (view_w, view_h, card_w, card_h,
                               n_cards, max, start, position);
}

static inline size_t
layout_trick (double view_w, double view_h, double card_w, double card_h,
              size_t n_players, size_t max, size_t start,
              TarotPosition * position)
{
  const double card_width_thin_view = view_w / (n_players - 1);
  const double card_height_wide_view = view_h / 2;
  const double card_scale_thin_view = card_width_thin_view / card_w;
  const double card_scale_wide_view = card_height_wide_view / card_h;
  const double card_scale =
    (card_scale_thin_view <
     card_scale_wide_view ? card_scale_thin_view : card_scale_wide_view);
  const double card_width = card_w * card_scale;
  const double card_height = card_h * card_scale;
  const double radius_x = (view_w - card_width) / 2;
  const double radius_y = (view_h - card_height) / 2;
  double n = n_players;
  size_t i;
  for (i = 0; i < max && i + start < n_players; i++)
    {
      const double angle =
        (i + start == 0 ? -M_PI / 2 : M_PI * (1 - (i + start) / n));
      const double x = radius_x * cos (angle);
      const double y = radius_y * sin (angle);
      const double z = (i + start + 1.0) / (n + 1.0);
      position[i].x = x;
      position[i].y = y;
      position[i].z = z;
      position[i].scale = card_scale;
      position[i].angle = 0;
    }
  return n_players;
}

static inline size_t
layout_center_horizontal (double view_w, double view_h, double card_w,
                          double card_h, size_t n_cards, size_t max,
                          size_t start, TarotPosition * position)
{
  const double card_scale = view_h / card_h;
  const double card_width = card_scale * card_w;
  const double margin = card_width / 2;
  const double amplitude = view_w / 2 - margin;
  double n = n_cards + 1;
  size_t i;
  for (i = 0; i < max && i + start < n_cards; i++)
    {
      const double pos = (i + start + 1) / n;
      const double position_sym = 2 * pos - 1;
      const double x = amplitude * position_sym;
      const double y = 0;
      const double z = pos;
      position[i].x = x;
      position[i].y = y;
      position[i].z = z;
      position[i].scale = 0.95 * card_scale;
      position[i].angle = 0;
    }
  return n_cards;
}

static inline size_t
layout_center_vertical (double view_w, double view_h, double card_w,
                        double card_h, size_t n_cards, size_t max,
                        size_t start, TarotPosition * position)
{
  size_t ret = layout_center_horizontal (view_h, view_w, card_h, card_w,
                                         n_cards, max, start, position);
  size_t i;
  for (i = 0; i < max && i + start < n_cards; i++)
    {
      position[i].y = -position[i].x;
      position[i].x = 0;
    }
  return ret;
}

static inline size_t
layout_center (double view_w, double view_h, double card_w, double card_h,
               size_t n_cards, size_t max, size_t start,
               TarotPosition * position)
{
  if (view_w / view_h > card_w / card_h)
    {
      return layout_center_horizontal (view_w, view_h, card_w, card_h,
                                       n_cards, max, start, position);
    }
  return layout_center_vertical (view_w, view_h, card_w, card_h, n_cards, max,
                                 start, position);
}

#endif /* not H_TAROT_LAYOUT_PRIVATE_IMPL_INCLUDED */
