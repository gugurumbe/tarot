/**
 * file tarot/tricks_private.h libtarot header for the management of
 * all tricks at once.
 *
 * Copyright (C) 2017, 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_TRICKS_PRIVATE_INCLUDED
#define H_TAROT_TRICKS_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/cards.h>
#include <tarot/player.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void tricks_3_generalize (TarotTricks3 * tricks,
                                   TarotTricksHandle * gen);
  static void tricks_4_generalize (TarotTricks4 * tricks,
                                   TarotTricksHandle * gen);
  static void tricks_5_generalize (TarotTricks5 * tricks,
                                   TarotTricksHandle * gen);
  static void tricks_initialize (TarotTricksHandle * tricks,
                                 size_t n_players);
  static int tricks_copy (TarotTricksHandle * dest,
                          const TarotTricksHandle * source);
  static size_t tricks_size (const TarotTricksHandle * tricks);
  static size_t tricks_current (const TarotTricksHandle * tricks);
  static size_t tricks_count_remaining_cards (const TarotTricksHandle *
                                              tricks, TarotPlayer who);
  static int tricks_has_next (const TarotTricksHandle * tricks);
  static TarotPlayer tricks_next (const TarotTricksHandle * tricks);
  static int tricks_points (const TarotTricksHandle * tricks,
                            TarotPlayer taker);
  static int tricks_oudlers (const TarotTricksHandle * tricks,
                             TarotPlayer taker);
  static int tricks_pab (const TarotTricksHandle * tricks, TarotPlayer taker,
                         TarotPlayer partner);
  static int tricks_slam (const TarotTricksHandle * tricks, TarotPlayer taker,
                          TarotPlayer partner);
  static int tricks_locate (const TarotTricksHandle * tricks,
                            TarotCard needle, size_t *i_trick,
                            size_t *position);
  static int tricks_set_leader (TarotTricksHandle * tricks,
                                TarotPlayer leader);
  static int tricks_add (TarotTricksHandle * tricks, TarotPlayer taker,
                         TarotPlayer partner, TarotCard card);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_TRICKS_PRIVATE_INCLUDED */
