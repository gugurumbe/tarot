namespace Tarot {
	public struct Card : uint {
		public static Tarot.Card parse (string text, out unowned string end);
		public static Tarot.Card parse_c (string text, out unowned string end);
		public size_t to_string (char dest[]);
		public size_t to_string_c (char dest[]);
		public string to_string_alloc ();
		public string to_string_c_alloc ();
	}
	public struct Player : uint {
		public static Tarot.Player parse (string text, out unowned string end);
		public static Tarot.Player parse_c (string text, out unowned string end);
		public size_t to_string (char dest[]);
		public size_t to_string_c (char dest[]);
		public string to_string_alloc ();
		public string to_string_c_alloc ();
	}
}