/**
 * file tarot/hands_private_impl.h libtarot code for the management of
 * all cards of all players in a game.
 *
 * Copyright (C) 2017, 2018, 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <tarot/hands_private.h>
#include <tarot/hand_private.h>

static inline int
_hands_get_known (const TarotHandsHandle * hands, unsigned int i)
{
  return hands->known[i];
}

static inline void
_hands_set_known (const TarotHandsHandle * hands, unsigned int i, int value)
{
  hands->known[i] = value;
}

static inline void
hands_3_generalize (TarotHands3 * hands, TarotHandsHandle * gen)
{
  size_t i;
  gen->n_players = 3;
  for (i = 0; i < gen->n_players; i++)
    {
      gen->hands[i].n_max =
        sizeof (hands->hands[i].buffer) / sizeof (TarotCard);
      gen->hands[i].n = &(hands->hands[i].n);
      gen->hands[i].buffer = hands->hands[i].buffer;
    }
  gen->known = hands->known;
}

static inline void
hands_4_generalize (TarotHands4 * hands, TarotHandsHandle * gen)
{
  size_t i;
  gen->n_players = 4;
  for (i = 0; i < gen->n_players; i++)
    {
      gen->hands[i].n_max =
        sizeof (hands->hands[i].buffer) / sizeof (TarotCard);
      gen->hands[i].n = &(hands->hands[i].n);
      gen->hands[i].buffer = hands->hands[i].buffer;
    }
  gen->known = hands->known;
}

static inline void
hands_5_generalize (TarotHands5 * hands, TarotHandsHandle * gen)
{
  size_t i;
  gen->n_players = 5;
  for (i = 0; i < gen->n_players; i++)
    {
      gen->hands[i].n_max =
        sizeof (hands->hands[i].buffer) / sizeof (TarotCard);
      gen->hands[i].n = &(hands->hands[i].n);
      gen->hands[i].buffer = hands->hands[i].buffer;
    }
  gen->known = hands->known;
}

static inline void
hands_initialize (TarotHandsHandle * hands, size_t n_players)
{
  size_t i = 0;
  hands->n_players = n_players;
  for (i = 0; i < n_players; ++i)
    {
      hand_set (&(hands->hands[i]), NULL, 0);
      _hands_set_known (hands, i, 0);
    }
}

static inline void
hands_copy (TarotHandsHandle * dest, const TarotHandsHandle * source)
{
  size_t i = 0;
  assert (dest->n_players == source->n_players);
  for (i = 0; i < source->n_players; ++i)
    {
      dest->known[i] = hands_known (source, i);
      if (hand_copy (&(dest->hands[i]), &(source->hands[i])) != 0)
        {
          assert (0);
        }
    }
}

static inline int
hands_known (const TarotHandsHandle * hands, TarotPlayer player)
{
  return (player < hands->n_players && _hands_get_known (hands, player));
}

static inline int
hands_set (TarotHandsHandle * hands, TarotPlayer player,
           const TarotHand * hand)
{
  if (player < hands->n_players)
    {
      int error = 0;
      error = hand_copy (&(hands->hands[player]), hand);
      if (error == 0)
        {
          _hands_set_known (hands, player, 1);
        }
      return error;
    }
  return 1;
}

static inline int
hands_set_unknown (TarotHandsHandle * hands, TarotPlayer player)
{
  if (player < hands->n_players)
    {
      _hands_set_known (hands, player, 0);
      return 0;
    }
  return 1;
}

static inline int
hands_set_all (TarotHandsHandle * hands, const TarotCard * owner_of)
{
  size_t i = 0;
  int error = 0;
  for (i = 0; i < hands->n_players; i++)
    {
      size_t needed;
      needed = hand_set_filter (&(hands->hands[i]), owner_of, i);
      error = (needed > hands->hands[i].n_max);
      _hands_set_known (hands, i, 1);
    }
  return error;
}

static inline void
hands_remove (TarotHandsHandle * hands, TarotPlayer player, TarotCard card)
{
  if (player < hands->n_players)
    {
      hand_remove (&(hands->hands[player]), card);
    }
}

static inline int
hands_assign_missing_card (TarotHandsHandle * hands, TarotCard card,
                           TarotPlayer player, size_t n_remaining)
{
  if (player < hands->n_players)
    {
      int error = 0;
      error = hand_insert (&(hands->hands[player]), card);
      if (error == 0 && hand_size (&(hands->hands[player])) >= n_remaining)
        {
          hands->known[player] = 1;
        }
      return error;
    }
  return 1;
}

static inline int
hands_locate (const TarotHandsHandle * hands, TarotCard card, TarotPlayer * p)
{
  int error = 1;
  size_t i;
  for (i = 0; i < hands->n_players; ++i)
    {
      if (_hands_get_known (hands, i))
        {
          if (hand_has (&(hands->hands[i]), card))
            {
              error = 0;
              *p = i;
            }
        }
    }
  return error;
}

#include <tarot/hand_private_impl.h>
