/**
 * file tarot/bid_private.h libtarot header for the bid functions.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_BID_PRIVATE_INCLUDED
#define H_TAROT_BID_PRIVATE_INCLUDED

#include "bid.h"

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  /**
   * Check whether a discard is allowed for a given bid.
   */
  static int bid_discard_allowed (TarotBid bid);

  /**
   * Check whether we should count the discard.
   */
  static int bid_discard_counted (TarotBid bid);

  /**
   * Check that newBid is either TAROT_PASS, or outbids base.
   */
  static int bid_check (TarotBid base, TarotBid newBid, int *OUTPUT);

  /**
   * Get the multiplier coefficient.
   */
  static int bid_multiplier (TarotBid bid);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_BID_PRIVATE_INCLUDED */
