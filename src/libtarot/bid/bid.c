/**
 * \file bid.c
 *
 * Copyright (C) 2017, 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "tarot/bid_private.h"

int
tarot_bid_discard_allowed (TarotBid bid)
{
  return bid_discard_allowed (bid);
}

int
tarot_bid_discard_counted (TarotBid bid)
{
  return bid_discard_counted (bid);
}

int
tarot_bid_check (TarotBid base, TarotBid new_bid, int *superior)
{
  return bid_check (base, new_bid, superior);
}

int
tarot_bid_multiplier (TarotBid bid)
{
  return bid_multiplier (bid);
}

#include "tarot/bid_private_impl.h"
