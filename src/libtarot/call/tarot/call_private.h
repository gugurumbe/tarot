/**
 * file tarot/call_private.h libtarot header for the management of the call.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_CALL_PRIVATE_INCLUDED
#define H_TAROT_CALL_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/player.h>
#include <tarot/cards.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void call_initialize (TarotCall * call);
  static int call_copy (TarotCall * dest, const TarotCall * source);
  static int call_start (TarotCall * call);
  static int call_waiting (const TarotCall * call);
  static int call_card_known (const TarotCall * call);
  static int call_partner_known (const TarotCall * call);
  static TarotCard call_card (const TarotCall * call);
  static TarotPlayer call_partner (const TarotCall * call);
  static int call_add (TarotCall * call, TarotPlayer taker, TarotCard c);
  static int call_owned_card (TarotCall * call, TarotPlayer owner,
                              TarotCard c);
  static int call_trick_card (TarotCall * call, TarotPlayer owner,
                              TarotCard c);
  static int call_doscard_card (TarotCall * call, TarotCard c);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_CALL_PRIVATE_INCLUDED */
