/*
 * file tarot.h Main libtarot header.
 *
 * Copyright (C) 2017, 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_INCLUDED
#define H_TAROT_INCLUDED

#include <tarot/bid.h>
#include <tarot/cards.h>
#include <tarot/player.h>
#include <tarot/step.h>
#include <tarot/game_event.h>
#include <tarot/game.h>
#include <tarot/counter.h>
#include <tarot/xml.h>
#include <tarot/layout.h>
#include <tarot/simulation.h>
#include <tarot/mcts.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  int tarot_init (const char *localedir);
  void tarot_quit (void);

  /**
   * tarot_libtarot_version:
   */
  const char *tarot_libtarot_version (void);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */
#endif                          /* not H_TAROT_INCLUDED */
