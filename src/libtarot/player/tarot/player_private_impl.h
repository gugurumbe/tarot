/**
 * file tarot/player_private_impl.h libtarot code for the management
 * of players.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <tarot/player_private.h>
#include "gettext.h"
#define _(String) dgettext (PACKAGE, String)
#define N_(String) (String)

static const char *names[6] = {
  N_("P1"),
  N_("P2"),
  N_("P3"),
  N_("P4"),
  N_("P5"),
  N_("P6")
};

static inline TarotPlayer
player_parse (const char *text, char **end)
{
  size_t i;
  TarotPlayer ret = ((TarotPlayer) (-1));
  for (i = 0; i < 6; i++)
    {
      const char *candidate = gettext (names[i]);
      if (strncmp (text, candidate, strlen (candidate)) == 0)
        {
          ret = (TarotPlayer) i;
          if (end)
            {
              *end = (char *) text + strlen (candidate);
            }
        }
    }
  if (ret == ((TarotPlayer) (-1)))
    {
      *end = (char *) text;
    }
  return ret;
}

static inline TarotPlayer
player_parse_c (const char *text, char **end)
{
  size_t i;
  TarotPlayer ret = ((TarotPlayer) (-1));
  for (i = 0; i < 6; i++)
    {
      const char *candidate = names[i];
      if (strncmp (text, candidate, strlen (candidate)) == 0)
        {
          ret = (TarotPlayer) i;
          if (end)
            {
              *end = (char *) text + strlen (candidate);
            }
        }
    }
  if (ret == ((TarotPlayer) (-1)))
    {
      *end = (char *) text;
    }
  return ret;
}

static inline size_t
player_to_string (TarotPlayer p, size_t max, char *dest)
{
  if (p >= 6)
    {
      if (max >= 1)
        {
          *dest = '\0';
        }
      return 0;
    }
  {
    const char *source = gettext (names[p]);
    if (max == 0)
      {
        return strlen (source);
      }
    else if (max > strlen (source))
      {
        strcpy (dest, source);
        return strlen (source);
      }
    else
      {
        strncpy (dest, source, max);
        dest[max - 1] = '\0';
        return strlen (source);
      }
  }
}

static inline size_t
player_to_string_c (TarotPlayer p, size_t max, char *dest)
{
  if (p >= 6)
    {
      if (max >= 1)
        {
          *dest = '\0';
        }
      return 0;
    }
  {
    const char *source = names[p];
    if (max == 0)
      {
        return strlen (source);
      }
    else if (max > strlen (source))
      {
        strcpy (dest, source);
        return strlen (source);
      }
    else
      {
        strncpy (dest, source, max);
        dest[max - 1] = '\0';
        return strlen (source);
      }
  }
}
