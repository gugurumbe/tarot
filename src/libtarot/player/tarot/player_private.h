/**
 * file tarot/player_private.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_PLAYER_PRIVATE_INCLUDED
#define H_TAROT_PLAYER_PRIVATE_INCLUDED

#include <tarot/player.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static TarotPlayer player_parse (const char *text, char **end);
  static size_t player_to_string (TarotPlayer p, size_t max, char *dest);

  static TarotPlayer player_parse_c (const char *text, char **end);
  static size_t player_to_string_c (TarotPlayer p, size_t max, char *dest);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_PLAYER_PRIVATE_INCLUDED */
