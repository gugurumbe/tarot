/**
 * file tarot/trick_private_impl.h libtarot code for the management of
 * a trick.
 *
 * Copyright (C) 2017, 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <tarot/trick_private.h>
#include <tarot/cards_private.h>
#include <assert.h>

static inline size_t
_trick_get_n_cards (const TarotTrickHandle * trick)
{
  return trick->trick->n_cards;
}

static inline void
_trick_set_n_cards (TarotTrickHandle * trick, size_t value)
{
  trick->trick->n_cards = value;
}

static inline int
_trick_get_last (const TarotTrickHandle * trick)
{
  return trick->trick->last;
}

static inline void
_trick_set_last (TarotTrickHandle * trick, int value)
{
  trick->trick->last = value;
}

static inline TarotPlayer
_trick_get_leader (const TarotTrickHandle * trick)
{
  return trick->trick->leader;
}

static inline void
_trick_set_leader (TarotTrickHandle * trick, TarotPlayer value)
{
  trick->trick->leader = value;
}

static inline int
_trick_get_has_forced_taker (const TarotTrickHandle * trick)
{
  return trick->trick->has_forced_taker;
}

static inline void
_trick_set_has_forced_taker (TarotTrickHandle * trick, int value)
{
  trick->trick->has_forced_taker = value;
}

static inline TarotPlayer
_trick_get_forced_taker (const TarotTrickHandle * trick)
{
  return trick->trick->forced_taker;
}

static inline void
_trick_set_forced_taker (TarotTrickHandle * trick, TarotPlayer value)
{
  trick->trick->forced_taker = value;
}

static inline TarotCard
_trick_get_card (const TarotTrickHandle * trick, size_t card)
{
  return trick->cards[card];
}

static inline void
_trick_set_card (TarotTrickHandle * trick, size_t card, TarotCard value)
{
  trick->cards[card] = value;
}

static inline void
trick_3_generalize (TarotTrick3 * trick, TarotTrickHandle * gen)
{
  gen->n_players = 3;
  gen->trick = &(trick->t);
  gen->cards = trick->cards;
}

static inline void
trick_4_generalize (TarotTrick4 * trick, TarotTrickHandle * gen)
{
  gen->n_players = 4;
  gen->trick = &(trick->t);
  gen->cards = trick->cards;
}

static inline void
trick_5_generalize (TarotTrick5 * trick, TarotTrickHandle * gen)
{
  gen->n_players = 5;
  gen->trick = &(trick->t);
  gen->cards = trick->cards;
}

static inline void
trick_initialize (TarotTrickHandle * trick, size_t n_players)
{
  trick->n_players = n_players;
  _trick_set_n_cards (trick, 0);
  _trick_set_last (trick, 0);
  _trick_set_leader (trick, n_players);
  assert (!trick_has_leader (trick));
  _trick_set_has_forced_taker (trick, 0);
}

static inline int
trick_copy (TarotTrickHandle * dest, const TarotTrickHandle * source)
{
  assert (dest->n_players == source->n_players);
  _trick_set_n_cards (dest, _trick_get_n_cards (source));
  _trick_set_last (dest, _trick_get_last (source));
  _trick_set_leader (dest, _trick_get_leader (source));
  _trick_set_has_forced_taker (dest, _trick_get_has_forced_taker (source));
  if (_trick_get_has_forced_taker (source))
    {
      _trick_set_forced_taker (dest, _trick_get_forced_taker (source));
    }
  memcpy (dest->cards, source->cards,
          _trick_get_n_cards (source) * sizeof (TarotCard));
  return 0;
}

static inline size_t
trick_n_cards (const TarotTrickHandle * trick)
{
  return _trick_get_n_cards (trick);
}

static inline int
trick_last (const TarotTrickHandle * trick)
{
  return _trick_get_last (trick);
}

static inline int
trick_set_last (TarotTrickHandle * trick, int last)
{
  _trick_set_last (trick, last);
  return 0;
}

static inline int
trick_has_leader (const TarotTrickHandle * trick)
{
  return (_trick_get_leader (trick) < trick->n_players);
}

static inline TarotPlayer
trick_leader (const TarotTrickHandle * trick)
{
  assert (trick_has_leader (trick));
  return _trick_get_leader (trick);
}

static inline int
trick_set_leader (TarotTrickHandle * trick, TarotPlayer leader)
{
  _trick_set_leader (trick, leader);
  return 0;
}

static inline int
trick_overtake (TarotCard before, TarotCard after)
{
  if (before == TAROT_EXCUSE)
    {
      return 1;
    }
  else if (after == TAROT_EXCUSE)
    {
      return 0;
    }
  else if (before < TAROT_PETIT && after < TAROT_PETIT)
    {
      TarotSuit sa, sb;
      TarotNumber na, nb;
      int error;
      error = decompose (before, &nb, &sb);
      assert (error == 0);
      error = decompose (after, &na, &sa);
      assert (error == 0);
      (void) error;
      return ((sa == sb) && (na > nb));
    }
  else if (before < TAROT_PETIT && after > TAROT_PETIT)
    {
      return 1;
    }
  else if (before > TAROT_PETIT && after < TAROT_PETIT)
    {
      return 0;
    }
  /* else if (before > TAROT_PETIT && after > TAROT_PETIT) */
  return (after > before);
}

static inline TarotPlayer
trick_taker (const TarotTrickHandle * trick)
{
  size_t i;
  size_t pos = 0;
  if (_trick_get_has_forced_taker (trick))
    {
      return _trick_get_forced_taker (trick);
    }
  if (!trick_has_leader (trick))
    {
      return trick->n_players;
    }
  for (i = 1; i < _trick_get_n_cards (trick); ++i)
    {
      TarotCard before = _trick_get_card (trick, pos);
      TarotCard after = _trick_get_card (trick, i);
      if (trick_overtake (before, after))
        {
          pos = i;
        }
    }
  return trick_get_player (trick, pos);
}

static inline TarotPlayer
trick_next (const TarotTrickHandle * trick)
{
  if (_trick_get_n_cards (trick) < trick->n_players)
    {
      TarotPlayer ret =
        _trick_get_leader (trick) + _trick_get_n_cards (trick);
      if (ret >= trick->n_players)
        {
          return ret - trick->n_players;
        }
      return ret;
    }
  return trick->n_players;
}

static inline int
trick_has_lead_suit (const TarotTrickHandle * trick)
{
  return (trick_has_leader (trick)
          && (_trick_get_n_cards (trick) > 1
              || (_trick_get_n_cards (trick) == 1
                  && _trick_get_card (trick, 0) != TAROT_EXCUSE)));
}

static inline TarotSuit
trick_lead_suit (const TarotTrickHandle * trick)
{
  int error;
  TarotNumber n;
  TarotSuit s;
  (void) error;
  assert (_trick_get_n_cards (trick) > 0);
  if (_trick_get_card (trick, 0) != TAROT_EXCUSE)
    {
      error = decompose (_trick_get_card (trick, 0), &n, &s);
      assert (error == 0);
      return s;
    }
  else
    {
      assert (_trick_get_n_cards (trick) > 1);
      error = tarot_decompose (_trick_get_card (trick, 1), &n, &s);
      assert (error == 0);
      return s;
    }
}

static inline TarotNumber
trick_max_trump (const TarotTrickHandle * trick)
{
  size_t i = 0;
  TarotNumber ret = 0;
  for (i = 0; i < _trick_get_n_cards (trick); i++)
    {
      if (_trick_get_card (trick, i) > TAROT_PETIT - 1 + ret
          && _trick_get_card (trick, i) < TAROT_EXCUSE)
        {
          ret = _trick_get_card (trick, i) - TAROT_PETIT + 1;
        }
    }
  return ret;
}


static inline int
trick_has_played (const TarotTrickHandle * trick, TarotPlayer player)
{
  if (trick_has_leader (trick))
    {
      size_t n = trick_get_position (trick, player);
      return (n < _trick_get_n_cards (trick));
    }
  return 0;
}

static inline TarotCard
trick_get_card_of (const TarotTrickHandle * trick, TarotPlayer player)
{
  size_t n = trick_get_position (trick, player);
  return _trick_get_card (trick, n);
}

static inline TarotCard
trick_get_card (const TarotTrickHandle * trick, size_t i)
{
  assert (i < _trick_get_n_cards (trick));
  return _trick_get_card (trick, i);
}

static inline TarotPlayer
trick_get_player (const TarotTrickHandle * trick, size_t i)
{
  TarotPlayer ret;
  assert (trick_has_leader (trick));
  ret = _trick_get_leader (trick) + i;
  if (ret >= trick->n_players)
    {
      return ret - trick->n_players;
    }
  return ret;
}

static inline size_t
trick_get_position (const TarotTrickHandle * trick, TarotPlayer player)
{
  assert (trick_has_leader (trick));
  if (_trick_get_leader (trick) <= player)
    {
      return player - _trick_get_leader (trick);
    }
  return player + trick->n_players - _trick_get_leader (trick);
}

static inline int
trick_cannot_follow (const TarotTrickHandle * trick, TarotPlayer p)
{
  TarotCard c;
  TarotNumber n;
  TarotSuit s;
  assert (trick_has_played (trick, p));
  c = trick_get_card_of (trick, p);
  if (c == TAROT_EXCUSE)
    {
      return 0;
    }
  else
    {
      int error = 0;
      error = decompose (c, &n, &s);
      (void) error;
      assert (error == 0);
    }
  return (s != trick_lead_suit (trick));
}

static inline int
trick_cannot_overtrump (const TarotTrickHandle * trick, TarotPlayer p)
{
  TarotCard c;
  TarotNumber n;
  TarotSuit s;
  size_t position;
  assert (trick_has_played (trick, p));
  position = trick_get_position (trick, p);
  c = _trick_get_card (trick, position);
  if (c == TAROT_EXCUSE)
    {
      return 0;
    }
  else
    {
      int error = 0;
      error = decompose (c, &n, &s);
      (void) error;
      assert (error == 0);
    }
  if (s != TAROT_TRUMPS)
    {
      return (trick_lead_suit (trick) != s);
    }
  else
    {
      size_t i_before;
      for (i_before = 0; i_before < position; i_before++)
        {
          TarotCard before = _trick_get_card (trick, i_before);
          if (before > c && before != TAROT_EXCUSE)
            {
              return 1;
            }
        }
    }
  return 0;
}

static inline int
trick_cannot_undertrump (const TarotTrickHandle * trick, TarotPlayer p)
{
  TarotCard c;
  TarotNumber n;
  TarotSuit s;
  size_t position;
  assert (trick_has_played (trick, p));
  position = trick_get_position (trick, p);
  c = _trick_get_card (trick, position);
  if (c == TAROT_EXCUSE)
    {
      return 0;
    }
  else
    {
      int error = 0;
      error = decompose (c, &n, &s);
      (void) error;
      assert (error == 0);
    }
  return (s != TAROT_TRUMPS && s != trick_lead_suit (trick));
}

static inline void
trick_check_card_full (const TarotTrickHandle * trick,
                       TarotCard played,
                       int *cnf, TarotSuit * arg_lead_suit,
                       int *cno, TarotNumber * arg_max_trump, int *cnu)
{
  if (cnf != NULL && arg_lead_suit != NULL && cno != NULL
      && arg_max_trump != NULL && cnu != NULL)
    {
      TarotNumber played_number;
      TarotSuit played_suit;
      int played_is_excuse = decompose (played, &played_number, &played_suit);
      TarotSuit lead_suit = (TarotSuit) 42;
      int has_lead_suit = trick_has_lead_suit (trick);
      TarotNumber max_trump = trick_max_trump (trick);
      if (has_lead_suit)
        {
          lead_suit = trick_lead_suit (trick);
        }
      *cnf = !played_is_excuse && has_lead_suit && played_suit != lead_suit;
      *cno = !played_is_excuse
        && has_lead_suit
        && ((played_suit == TAROT_TRUMPS && played_number < max_trump)
            || (played_suit != TAROT_TRUMPS && played_suit != lead_suit));
      *cnu = !played_is_excuse && played_suit != TAROT_TRUMPS
        && has_lead_suit && played_suit != lead_suit;
      if (has_lead_suit)
        {
          *arg_lead_suit = lead_suit;
        }
      *arg_max_trump = max_trump;
    }
}

static inline TarotPlayer
_trick_inc (TarotPlayer p, size_t n)
{
  TarotPlayer ret = p + 1;
  if (ret >= n)
    {
      ret -= n;
    }
  return ret;
}

static inline void
trick_points_and_oudlers (const TarotTrickHandle * trick, int *hp,
                          int *oudlers)
{
  size_t i = 0;
  TarotPlayer taker = trick_taker (trick);
  TarotPlayer ip = trick_leader (trick);
  *hp = 0;
  *oudlers = 0;
  for (i = 0; i < _trick_get_n_cards (trick);
       ++i, ip = _trick_inc (ip, trick->n_players))
    {
      TarotCard c = _trick_get_card (trick, i);
      TarotNumber n;
      TarotSuit s;
      int error;
      if (c == TAROT_PETIT
          || c == TAROT_TWENTYONE
          || (c == TAROT_EXCUSE && (_trick_get_last (trick) || ip == taker)))
        {
          *oudlers = *oudlers + 1;
          *hp = *hp + 9;
        }
      else if (c == TAROT_EXCUSE)
        {
          *hp = *hp + 1;
        }
      else
        {
          error = decompose (c, &n, &s);
          (void) error;
          assert (error == 0);
          *hp = *hp + 1;
          if (s != TAROT_TRUMPS && n > 10)
            {
              *hp = *hp + 2 * (n - 10);
            }
        }
    }
}

static inline int
trick_points (const TarotTrickHandle * trick)
{
  int hp, od;
  trick_points_and_oudlers (trick, &hp, &od);
  return hp;
}

static inline int
trick_oudlers (const TarotTrickHandle * trick)
{
  int hp, od;
  trick_points_and_oudlers (trick, &hp, &od);
  return od;
}

static inline int
trick_has_petit (const TarotTrickHandle * trick)
{
  size_t i;
  for (i = 0; i < _trick_get_n_cards (trick); ++i)
    {
      if (_trick_get_card (trick, i) == TAROT_PETIT)
        {
          return 1;
        }
    }
  return 0;
}

static inline int
trick_taken_by_excuse (const TarotTrickHandle * trick)
{
  return ((_trick_get_has_forced_taker (trick))
          && (trick_get_card_of (trick, trick_taker (trick))
              == TAROT_EXCUSE));
}

static inline int
trick_locate (const TarotTrickHandle * trick, TarotCard needle,
              size_t *position)
{
  size_t i;
  int ret = 1;
  for (i = 0; i < _trick_get_n_cards (trick); ++i)
    {
      if (_trick_get_card (trick, i) == needle)
        {
          *position = i;
          ret = 0;
        }
    }
  return ret;
}

static inline int
trick_add (TarotTrickHandle * trick, TarotCard card, int force_taker)
{
  if (_trick_get_n_cards (trick) >= trick->n_players)
    {
      return 1;
    }
  if (force_taker)
    {
      _trick_set_has_forced_taker (trick, 1);
      _trick_set_forced_taker (trick, trick_next (trick));
    }
  _trick_set_card (trick, _trick_get_n_cards (trick), card);
  _trick_set_n_cards (trick, 1 + _trick_get_n_cards (trick));
  return 0;
}

#include <tarot/cards_private_impl.h>
