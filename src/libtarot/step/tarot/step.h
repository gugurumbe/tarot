/*
 * file tarot/step.h libtarot header for the definition of the game
 * steps.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_STEP_INCLUDED
#define H_TAROT_STEP_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  /**
   * TarotStep:
   */
  typedef enum
  {
    TAROT_SETUP,
    TAROT_DEAL,
    TAROT_BIDS,
    TAROT_DECLS,
    TAROT_CALL,
    TAROT_DOG,
    TAROT_DISCARD,
    TAROT_TRICKS,
    TAROT_END
  }
  TarotStep;

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_STEP_INCLUDED */
