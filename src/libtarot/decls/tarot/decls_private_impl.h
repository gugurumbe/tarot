/**
 * file tarot/decls_private_impl libtarot code for the management of
 * the round of slam declarations.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <tarot/decls_private.h>

static inline size_t
_decls_get_n_decls (const TarotDecls * decls)
{
  return decls->n_decls;
}

static inline void
_decls_set_n_decls (TarotDecls * decls, size_t n_decls)
{
  decls->n_decls = n_decls;
}

static inline int
_decls_get_has_started (const TarotDecls * decls)
{
  return decls->has_started;
}

static inline void
_decls_set_has_started (TarotDecls * decls, int has_started)
{
  decls->has_started = has_started;
}

static inline TarotPlayer
_decls_get_slammer (const TarotDecls * decls)
{
  return decls->slammer;
}

static inline void
_decls_set_slammer (TarotDecls * decls, TarotPlayer slammer)
{
  decls->slammer = slammer;
}

static inline void
decls_initialize (TarotDecls * decls, size_t n_players)
{
  _decls_set_n_decls (decls, 0);
  _decls_set_has_started (decls, 0);
  _decls_set_slammer (decls, n_players);
}

static inline int
decls_copy (TarotDecls * dest, const TarotDecls * source)
{
  _decls_set_has_started (dest, _decls_get_has_started (source));
  _decls_set_n_decls (dest, _decls_get_n_decls (source));
  _decls_set_slammer (dest, _decls_get_slammer (source));
  return 0;
}

static inline int
decls_start (TarotDecls * decls)
{
  int error = _decls_get_has_started (decls);
  if (error == 0)
    {
      _decls_set_has_started (decls, 1);
    }
  return error;
}

static inline int
decls_has_next (const TarotDecls * decls, size_t n_players)
{
  return _decls_get_has_started (decls)
    && (_decls_get_n_decls (decls) < n_players);
}

static inline TarotPlayer
decls_next (const TarotDecls * decls)
{
  return _decls_get_n_decls (decls);
}

static inline int
decls_has_declarant (const TarotDecls * decls, size_t n_players)
{
  return (_decls_get_has_started (decls)
          && !decls_has_next (decls, n_players)
          && _decls_get_slammer (decls) < _decls_get_n_decls (decls));
}

static inline TarotPlayer
decls_declarant (const TarotDecls * decls)
{
  return _decls_get_slammer (decls);
}

static inline int
decls_check (const TarotDecls * decls, int next, size_t n_players)
{
  (void) next;
  return decls_has_next (decls, n_players);
}

static inline int
decls_add (TarotDecls * decls, int next, size_t n_players)
{
  int ok;
  ok = decls_check (decls, next, n_players);
  if (!ok)
    {
      return 1;
    }
  if (next)
    {
      _decls_set_slammer (decls, _decls_get_n_decls (decls));
      _decls_set_n_decls (decls, n_players);
    }
  else
    {
      _decls_set_n_decls (decls, _decls_get_n_decls (decls) + 1);
    }
  return 0;
}

static inline int
decls_done (const TarotDecls * decls, size_t n_players)
{
  return (_decls_get_has_started (decls)
          && _decls_get_n_decls (decls) == n_players);
}
