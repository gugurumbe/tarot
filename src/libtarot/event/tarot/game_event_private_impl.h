/**
 * file tarot/game_event_private_impl.h libtarot code for the
 * management of an event.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GAME_EVENT_PRIVATE_IMPL_INCLUDED
#define H_TAROT_GAME_EVENT_PRIVATE_IMPL_INCLUDED

#include <tarot/game_event_private.h>
#include <assert.h>
#include <stdlib.h>
#include <nettle/yarrow.h>

static inline size_t
event_size (size_t n_data)
{
  return sizeof (TarotGameEvent) + n_data * sizeof (unsigned int);
}

static inline void
event_copy_data (TarotGameEvent * event, size_t n, const unsigned int *data)
{
  memcpy (event->data, data, n * sizeof (*data));
  event->n = n;
}

static inline void
event_copy (TarotGameEvent * dest, const TarotGameEvent * source)
{
  dest->t = source->t;
  dest->u = source->u;
  event_copy_data (dest, source->n, source->data);
}

static inline TarotGameEventT
event_type (const TarotGameEvent * event)
{
  return event->t;
}

static inline void
event_set_setup (TarotGameEvent * event, size_t n_players, int with_call)
{
  event->t = TAROT_SETUP_EVENT;
  event->u.setup.n_players = n_players;
  event->u.setup.with_call = with_call;
  event_copy_data (event, 0, NULL);
}

static inline void
event_set_deal (TarotGameEvent * event, TarotPlayer myself, size_t n_cards,
                const TarotCard * cards)
{
  event->t = TAROT_DEAL_EVENT;
  event->u.deal = myself;
  event_copy_data (event, n_cards, cards);
}

static inline void
event_set_deal_all (TarotGameEvent * event, size_t n_owners,
                    const TarotPlayer * owners)
{
  event->t = TAROT_DEAL_ALL_EVENT;
  event_copy_data (event, n_owners, owners);
}

static inline void
event_set_deal_all_random (TarotGameEvent * event, size_t n_players,
                           size_t seed_size, const void *seed)
{
  unsigned int short_seed = 0;
  size_t short_seed_size = sizeof (short_seed);
  size_t n_cards;
  unsigned int i;
  TarotPlayer owners[78];
  struct yarrow256_ctx generator;
  switch (n_players)
    {
    case 3:
      n_cards = 24;
      break;
    case 4:
      n_cards = 18;
      break;
    case 5:
      n_cards = 15;
      break;
    default:
      assert (0);
    }
  if (short_seed_size > seed_size)
    {
      short_seed_size = seed_size;
    }
  memcpy (&short_seed, seed, short_seed_size);
  yarrow256_init (&generator, 0, NULL);
  yarrow256_seed (&generator, seed_size, seed);
  for (i = 0; i < 78; i++)
    {
      owners[i] = i / n_cards;
    }
  for (i = 1; i < 78; i++)
    {
      unsigned char random_buffer[sizeof (unsigned long)];
      unsigned long random_number;
      yarrow256_random (&generator, sizeof (unsigned long), random_buffer);
      memcpy (&random_number, random_buffer, sizeof (unsigned long));
      if (1)
        {
          unsigned int pos = random_number % (i + 1);
          TarotPlayer hold = owners[pos];
          owners[pos] = owners[i];
          owners[i] = hold;
        }
    }
  return event_set_deal_all (event, 78, owners);
}

static inline void
event_set_bid (TarotGameEvent * event, TarotBid bid)
{
  event->t = TAROT_BID_EVENT;
  event->u.bid = bid;
  event_copy_data (event, 0, NULL);
}

static inline void
event_set_decl (TarotGameEvent * event, int slam)
{
  event->t = TAROT_DECL_EVENT;
  event->u.decl = slam;
  event_copy_data (event, 0, NULL);
}

static inline void
event_set_call (TarotGameEvent * event, TarotCard card)
{
  event->t = TAROT_CALL_EVENT;
  event->u.call = card;
  event_copy_data (event, 0, NULL);
}

static inline void
event_set_dog (TarotGameEvent * event, size_t n_cards,
               const TarotCard * cards)
{
  event->t = TAROT_DOG_EVENT;
  event_copy_data (event, n_cards, cards);
}

static inline void
event_set_discard (TarotGameEvent * event, size_t n_cards,
                   const TarotCard * cards)
{
  event->t = TAROT_DISCARD_EVENT;
  event_copy_data (event, n_cards, cards);
}

static inline void
event_set_handful (TarotGameEvent * event, size_t n_cards,
                   const TarotCard * cards)
{
  event->t = TAROT_HANDFUL_EVENT;
  event_copy_data (event, n_cards, cards);
}

static inline void
event_set_card (TarotGameEvent * event, TarotCard card)
{
  assert (card < 78);
  event->t = TAROT_CARD_EVENT;
  event->u.card = card;
  event_copy_data (event, 0, NULL);
}

typedef struct
{
  TarotGameEvent event;
  unsigned int data[0];
} EventWithData;

static inline size_t
event_construct_setup (size_t max_mem, char *mem, size_t *alignment,
                       size_t n_players, int with_call)
{
  size_t required = sizeof (EventWithData);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_setup (&(e->event), n_players, with_call);
    }
  return required;
}

static inline size_t
event_construct_deal (size_t max_mem, char *mem, size_t *alignment,
                      TarotPlayer myself, size_t n_cards,
                      const TarotCard * cards)
{
  size_t required = sizeof (EventWithData) + n_cards * sizeof (unsigned int);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_deal (&(e->event), myself, n_cards, cards);
    }
  return required;
}

static inline size_t
event_construct_deal_all (size_t max_mem, char *mem, size_t *alignment,
                          size_t n_owners, const TarotPlayer * owners)
{
  size_t required =
    (sizeof (EventWithData) + n_owners * sizeof (unsigned int));
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_deal_all (&(e->event), n_owners, owners);
    }
  return required;
}

static inline size_t
event_construct_deal_all_random (size_t max_mem, char *mem, size_t *alignment,
                                 size_t n_players, size_t seed_size,
                                 const void *seed)
{
  size_t required = sizeof (EventWithData) + 78 * sizeof (unsigned int);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_deal_all_random (&(e->event), n_players, seed_size, seed);
    }
  return required;
}

static inline size_t
event_construct_bid (size_t max_mem, char *mem, size_t *alignment,
                     TarotBid bid)
{
  size_t required = sizeof (EventWithData);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_bid (&(e->event), bid);
    }
  return required;
}

static inline size_t
event_construct_decl (size_t max_mem, char *mem, size_t *alignment, int decl)
{
  size_t required = sizeof (EventWithData);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_decl (&(e->event), decl);
    }
  return required;
}

static inline size_t
event_construct_call (size_t max_mem, char *mem, size_t *alignment,
                      TarotCard call)
{
  size_t required = sizeof (EventWithData);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_call (&(e->event), call);
    }
  return required;
}

static inline size_t
event_construct_dog (size_t max_mem, char *mem, size_t *alignment,
                     size_t n_cards, const TarotCard * cards)
{
  size_t required = sizeof (EventWithData) + n_cards * sizeof (unsigned int);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_dog (&(e->event), n_cards, cards);
    }
  return required;
}

static inline size_t
event_construct_discard (size_t max_mem, char *mem, size_t *alignment,
                         size_t n_cards, const TarotCard * cards)
{
  size_t required = sizeof (EventWithData) + n_cards * sizeof (unsigned int);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      assert ((char *) e->event.data + n_cards * sizeof (unsigned int) ==
              (char *) mem + required);
      event_set_discard (&(e->event), n_cards, cards);
    }
  return required;
}

static inline size_t
event_construct_handful (size_t max_mem, char *mem, size_t *alignment,
                         size_t n_cards, const TarotCard * cards)
{
  size_t required = sizeof (EventWithData) + n_cards * sizeof (unsigned int);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_handful (&(e->event), n_cards, cards);
    }
  return required;
}

static inline size_t
event_construct_card (size_t max_mem, char *mem, size_t *alignment,
                      TarotCard card)
{
  size_t required = sizeof (EventWithData);
  *alignment = alignof (EventWithData);
  if (required <= max_mem)
    {
      assert (((size_t) mem) % (*alignment) == 0);
    }
  if (required <= max_mem)
    {
      EventWithData *e = (EventWithData *) mem;
      e->event.data = e->data;
      event_set_card (&(e->event), card);
    }
  return required;
}

static inline void
event_get_data (const TarotGameEvent * event, size_t *n, size_t start,
                size_t max, TarotCard * cards)
{
  *n = event->n;
  memcpy (cards, event->data + start, max * sizeof (*cards));
}

static inline TarotGameEventError
event_get_setup (const TarotGameEvent * event, size_t *n_players,
                 int *with_call)
{
  if (event->t == TAROT_SETUP_EVENT)
    {
      *n_players = event->u.setup.n_players;
      *with_call = event->u.setup.with_call;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_deal (const TarotGameEvent * event, TarotPlayer * myself,
                size_t *n_cards, size_t start, size_t max, TarotCard * cards)
{
  int ok = (event->t == TAROT_DEAL_EVENT);
  if (ok)
    {
      *myself = event->u.deal;
      event_get_data (event, n_cards, start, max, cards);
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_deal_all (const TarotGameEvent * event, size_t *n_owners,
                    size_t start, size_t max, TarotPlayer * players)
{
  int ok = (event->t == TAROT_DEAL_ALL_EVENT);
  if (ok)
    {
      event_get_data (event, n_owners, start, max, players);
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_bid (const TarotGameEvent * event, TarotBid * bid)
{
  int ok = (event->t == TAROT_BID_EVENT);
  if (ok)
    {
      *bid = event->u.bid;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_decl (const TarotGameEvent * event, int *decl)
{
  int ok = (event->t == TAROT_DECL_EVENT);
  if (ok)
    {
      *decl = event->u.decl;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_call (const TarotGameEvent * event, TarotCard * call)
{
  int ok = (event->t == TAROT_CALL_EVENT);
  if (ok)
    {
      *call = event->u.call;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_dog (const TarotGameEvent * event, size_t *n_cards, size_t start,
               size_t max, TarotCard * cards)
{
  int ok = (event->t == TAROT_DOG_EVENT);
  if (ok)
    {
      event_get_data (event, n_cards, start, max, cards);
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_discard (const TarotGameEvent * event, size_t *n_cards,
                   size_t start, size_t max, TarotCard * cards)
{
  int ok = (event->t == TAROT_DISCARD_EVENT);
  if (ok)
    {
      event_get_data (event, n_cards, start, max, cards);
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_handful (const TarotGameEvent * event, size_t *n_cards,
                   size_t start, size_t max, TarotCard * cards)
{
  int ok = (event->t == TAROT_HANDFUL_EVENT);
  if (ok)
    {
      event_get_data (event, n_cards, start, max, cards);
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_card (const TarotGameEvent * event, TarotCard * card)
{
  int ok = (event->t == TAROT_CARD_EVENT);
  if (ok)
    {
      *card = event->u.card;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

#endif /* not H_TAROT_GAME_EVENT_PRIVATE_IMPL_INCLUDED */
