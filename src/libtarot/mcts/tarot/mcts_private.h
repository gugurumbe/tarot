/**
 * file mcts_private.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef H_TAROT_MCTS_PRIVATE_INCLUDED
#define H_TAROT_MCTS_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/mcts.h>
#include <tarot/game_event.h>
#include <nettle/yarrow.h>
#include <tarot/game.h>
#include <tarot/counter.h>
#include <stdlib.h>
#include <assert.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static size_t mcts_construct (size_t max_mem, char *mem, size_t *alignment,
                                size_t pool_size);
  static void mcts_copy (TarotMcts * mcts, const TarotMcts * source);
  static TarotMctsError mcts_set_base (TarotMcts * mcts,
                                       const TarotGame * base);
  static void mcts_seed (TarotMcts * mcts, size_t seed_size,
                         const void *seed);
  static TarotMctsError mcts_run (TarotMcts * mcts, size_t n_iterations);
  static void mcts_set_parameter (TarotMcts * mcts, double value);
  static double mcts_get_parameter (const TarotMcts * mcts);
  static void mcts_set_simulation_random (TarotMcts * mcts, double value);
  static double mcts_get_simulation_random (const TarotMcts * mcts);
  static void mcts_set_simulation_agreed (TarotMcts * mcts, double value);
  static double mcts_get_simulation_agreed (const TarotMcts * mcts);
  static size_t mcts_default_iterations ();
  static const TarotGameEvent *mcts_get_best (const TarotMcts * mcts);
  static TarotMctsNode *mcts_root (TarotMcts * mcts);
  static const TarotGameEvent *mcts_get (const TarotMctsNode * node,
                                         size_t *n_simulations,
                                         int *sum_scores);
  static TarotMctsNode *mcts_parent (TarotMctsNode * node);
  static TarotMctsNode *mcts_first_child (TarotMctsNode * node);
  static TarotMctsNode *mcts_previous_sibling (TarotMctsNode * node);
  static TarotMctsNode *mcts_next_sibling (TarotMctsNode * node);
#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_MCTS_PRIVATE_INCLUDED */

/* Local Variables: */
/* mode: c */
/* End: */
