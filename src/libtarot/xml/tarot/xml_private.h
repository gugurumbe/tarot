/**
 * file tarot/xml_private.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_XML_PRIVATE_INCLUDED
#define H_TAROT_XML_PRIVATE_INCLUDED

#include <tarot/xml.h>
#include <tarot/game_event_private.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static int xml_construct (TarotXmlParser * parser, const char *input);
  static void xml_destruct (TarotXmlParser * parser);

  static int xml_iterator_construct (TarotXmlParserIterator * iterator,
                                     const TarotXmlParser * parser);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_XML_PRIVATE_INCLUDED */
