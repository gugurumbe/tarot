/**
 * file tarot/xml_private_impl.h
 *
 * Copyright (C) 2018, 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_XML_PRIVATE_IMPL_INCLUDED
#define H_TAROT_XML_PRIVATE_IMPL_INCLUDED

#include <tarot/xml_private.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <tarot/cards_private.h>
#include <tarot/player_private.h>

static inline int
xml_construct (TarotXmlParser * parser, const char *input)
{
  parser->document = NULL;
  parser->root = NULL;
  parser->document =
    xmlReadMemory (input, strlen (input), NULL, NULL, XML_PARSE_NOBLANKS);
  if (parser->document == NULL)
    {
      xml_destruct (parser);
      return 1;
    }
  parser->root = xmlDocGetRootElement (parser->document);
  if (parser->root->ns == NULL
      || parser->root->ns->href == NULL
      || strcmp ((const char *) parser->root->ns->href,
                 "http://planete-kraus.eu/tarot") != 0)
    {
      xml_destruct (parser);
      return 1;
    }
  if (strcmp ((const char *) parser->root->name, "game") != 0)
    {
      xml_destruct (parser);
      return 1;
    }
  return 0;
}

static inline void
xml_destruct (TarotXmlParser * parser)
{
  if (parser != NULL)
    {
      xmlFreeDoc (parser->document);
      parser->document = NULL;
      parser->root = NULL;
    }
}

static int
parse_prop_number (xmlNodePtr node, const char *name, size_t *value)
{
  xmlChar *prop = xmlGetProp (node, (const xmlChar *) name);
  const char *str = (const char *) prop;
  char *end = NULL;
  if (str == NULL)
    {
      return 1;
    }
  *value = strtoul (str, &end, 10);
  if (end == (char *) str || *end != '\0')
    {
      xmlFree (prop);
      return 1;
    }
  xmlFree (prop);
  return 0;
}

static int
parse_prop_boolean (xmlNodePtr node, const char *name, int *value)
{
  xmlChar *prop = xmlGetProp (node, (const xmlChar *) name);
  const char *str = (const char *) prop;
  if (str == NULL)
    {
      return 1;
    }
  if (strcmp (str, "yes") == 0)
    {
      *value = 1;
    }
  else if (strcmp (str, "no") == 0)
    {
      *value = 0;
    }
  else
    {
      xmlFree (prop);
      return 1;
    }
  xmlFree (prop);
  return 0;
}

static inline int
setup_setup (TarotXmlParserIterator * iterator)
{
  size_t n_players;
  int with_call;
  if (parse_prop_number (iterator->current, "n-players", &n_players) == 0
      && parse_prop_boolean (iterator->current, "with-call", &with_call) == 0)
    {
      event_set_setup (&(iterator->event), n_players, with_call);
      return 0;
    }
  return 1;
}

static int
parse_prop_card (xmlNodePtr node, const char *name, TarotCard * value)
{
  xmlChar *prop = xmlGetProp (node, (const xmlChar *) name);
  const char *str = (const char *) prop;
  char *end = NULL;
  if (str == NULL)
    {
      return 1;
    }
  *value = tarot_card_parse_c (str, &end);
  if (end == (char *) str || *end != '\0')
    {
      xmlFree (prop);
      return 1;
    }
  xmlFree (prop);
  return 0;
}

static int
parse_card (xmlNodePtr node, TarotCard * parsed)
{
  if (strcmp ((const char *) node->name, "card") == 0)
    {
      return parse_prop_card (node, "card", parsed);
    }
  return 1;
}

static int
parse_cards (xmlNodePtr node, size_t *n_cards, size_t max_cards,
             TarotCard * buffer)
{
  size_t n_children = 0;
  xmlNodePtr child = node->xmlChildrenNode;
  while (child != NULL)
    {
      n_children++;
      child = child->next;
    }
  *n_cards = n_children;
  if (n_children > max_cards)
    {
      return 1;
    }
  n_children = 0;
  child = node->xmlChildrenNode;
  while (child != NULL)
    {
      TarotCard parsed;
      if (parse_card (child, &parsed) != 0)
        {
          *n_cards = 0;
          return 1;
        }
      buffer[n_children++] = parsed;
      child = child->next;
    }
  assert (*n_cards == n_children);
  return 0;
}

static int
parse_prop_player (xmlNodePtr node, const char *name, TarotPlayer * value)
{
  xmlChar *prop = xmlGetProp (node, (const xmlChar *) name);
  const char *str = (const char *) prop;
  char *end = NULL;
  if (str == NULL)
    {
      return 1;
    }
  *value = tarot_player_parse_c (str, &end);
  if (end == (char *) str || *end != '\0')
    {
      xmlFree (prop);
      return 1;
    }
  xmlFree (prop);
  return 0;
}

static inline int
setup_deal (TarotXmlParserIterator * iterator)
{
  TarotCard buffer[78];
  size_t n_cards;
  TarotPlayer to;
  if (parse_prop_player (iterator->current, "to", &to) == 0
      && parse_cards (iterator->current, &n_cards, 78, buffer) == 0)
    {
      TarotGameEvent *e;
      if (n_cards > 78)
        {
          return 1;
        }
      iterator->event.n = n_cards;
      iterator->event.data = iterator->event_data;
      e = &(iterator->event);
      event_set_deal (e, to, n_cards, buffer);
      return 0;
    }
  return 1;
}

static int
parse_player (xmlNodePtr node, TarotPlayer * parsed)
{
  if (strcmp ((const char *) node->name, "player") == 0)
    {
      return parse_prop_player (node, "player", parsed);
    }
  return 1;
}

static int
parse_players (xmlNodePtr node, size_t *n_players, size_t max_players,
               TarotPlayer * buffer)
{
  size_t n_children = 0;
  xmlNodePtr child = node->xmlChildrenNode;
  while (child != NULL)
    {
      n_children++;
      child = child->next;
    }
  *n_players = n_children;
  if (n_children > max_players)
    {
      return 1;
    }
  n_children = 0;
  child = node->xmlChildrenNode;
  while (child != NULL)
    {
      TarotPlayer parsed;
      if (parse_player (child, &parsed) != 0)
        {
          *n_players = 0;
          return 1;
        }
      buffer[n_children++] = parsed;
      child = child->next;
    }
  return 0;
}

static inline int
setup_deal_all (TarotXmlParserIterator * iterator)
{
  TarotPlayer buffer[78];
  size_t n_players;
  if (parse_players (iterator->current, &n_players, 78, buffer) == 0)
    {
      if (n_players > 78)
        {
          return 1;
        }
      iterator->event.n = n_players;
      iterator->event.data = iterator->event_data;
      event_set_deal_all (&(iterator->event), n_players, buffer);
      return 0;
    }
  return 1;
}

static int
parse_prop_bid (xmlNodePtr node, const char *name, TarotBid * value)
{
  xmlChar *prop = xmlGetProp (node, (const xmlChar *) name);
  const char *str = (const char *) prop;
  int ret = 1;
  if (str == NULL)
    {
      return 1;
    }
  if (strcmp (str, "pass") == 0)
    {
      *value = TAROT_PASS;
      ret = 0;
    }
  if (strcmp (str, "take") == 0)
    {
      *value = TAROT_TAKE;
      ret = 0;
    }
  if (strcmp (str, "push") == 0)
    {
      *value = TAROT_PUSH;
      ret = 0;
    }
  if (strcmp (str, "straight-keep") == 0)
    {
      *value = TAROT_STRAIGHT_KEEP;
      ret = 0;
    }
  if (strcmp (str, "double-keep") == 0)
    {
      *value = TAROT_DOUBLE_KEEP;
      ret = 0;
    }
  xmlFree (prop);
  return ret;
}

static inline int
setup_bid (TarotXmlParserIterator * iterator)
{
  TarotBid parsed;
  if (parse_prop_bid (iterator->current, "bid", &parsed) == 0)
    {
      iterator->event.n = 0;
      event_set_bid (&(iterator->event), parsed);
      return 0;
    }
  return 1;
}

static inline int
setup_decl (TarotXmlParserIterator * iterator)
{
  int parsed;
  if (parse_prop_boolean (iterator->current, "slam", &parsed) == 0)
    {
      iterator->event.n = 0;
      event_set_decl (&(iterator->event), parsed);
      return 0;
    }
  return 1;
}

static inline int
setup_call (TarotXmlParserIterator * iterator)
{
  TarotCard parsed;
  if (parse_prop_card (iterator->current, "card", &parsed) == 0)
    {
      iterator->event.n = 0;
      event_set_call (&(iterator->event), parsed);
      return 0;
    }
  return 1;
}

static inline int
setup_dog (TarotXmlParserIterator * iterator)
{
  TarotCard buffer[78];
  size_t n_cards;
  if (parse_cards (iterator->current, &n_cards, 78, buffer) == 0)
    {
      if (n_cards > 78)
        {
          return 1;
        }
      iterator->event.n = n_cards;
      iterator->event.data = iterator->event_data;
      event_set_dog (&(iterator->event), n_cards, buffer);
      return 0;
    }
  return 1;
}

static inline int
setup_discard (TarotXmlParserIterator * iterator)
{
  TarotCard buffer[78];
  size_t n_cards;
  if (parse_cards (iterator->current, &n_cards, 78, buffer) == 0)
    {
      if (n_cards > 78)
        {
          return 1;
        }
      iterator->event.n = n_cards;
      iterator->event.data = iterator->event_data;
      event_set_discard (&(iterator->event), n_cards, buffer);
      return 0;
    }
  return 1;
}

static inline int
setup_handful (TarotXmlParserIterator * iterator)
{
  TarotCard buffer[78];
  size_t n_cards;
  if (parse_cards (iterator->current, &n_cards, 78, buffer) == 0)
    {
      if (n_cards > 78)
        {
          return 1;
        }
      iterator->event.n = n_cards;
      iterator->event.data = iterator->event_data;
      event_set_handful (&(iterator->event), n_cards, buffer);
      return 0;
    }
  return 1;
}

static inline int
setup_card (TarotXmlParserIterator * iterator)
{
  TarotCard parsed;
  if (parse_prop_card (iterator->current, "card", &parsed) == 0)
    {
      iterator->event.n = 0;
      event_set_card (&(iterator->event), parsed);
      return 0;
    }
  return 1;
}

static inline int
setup_contents (TarotXmlParserIterator * iterator)
{
  if (iterator->current->ns == NULL || iterator->current->ns->href == NULL)
    {
      return 1;
    }
  if (strcmp ((const char *) iterator->current->ns->href,
              "http://planete-kraus.eu/tarot") != 0)
    {
      return 1;
    }
  if (strcmp ((const char *) iterator->current->name, "setup") == 0)
    {
      return setup_setup (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "deal") == 0)
    {
      return setup_deal (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "deal-all") == 0)
    {
      return setup_deal_all (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "bid") == 0)
    {
      return setup_bid (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "decl") == 0)
    {
      return setup_decl (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "call") == 0)
    {
      return setup_call (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "dog") == 0)
    {
      return setup_dog (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "discard") == 0)
    {
      return setup_discard (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "handful") == 0)
    {
      return setup_handful (iterator);
    }
  else if (strcmp ((const char *) iterator->current->name, "card") == 0)
    {
      return setup_card (iterator);
    }
  else
    {
      return 1;
    }
}

static inline int
xml_iterator_construct (TarotXmlParserIterator * iterator,
                        const TarotXmlParser * parser)
{
  iterator->current = parser->root->children;
  iterator->event.n = 0;
  return setup_contents (iterator);
}

#include <tarot/cards_private_impl.h>
#include <tarot/player_private_impl.h>
#include <tarot/game_event_private_impl.h>
#endif /* not H_TAROT_XML_PRIVATE_INCLUDED */
