/**
 * file check_xml.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/game_event.h>
#include <tarot/game.h>
#include <tarot/xml.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "xalloc.h"
#include <stdalign.h>

static void
unit_test (const char *xml)
{
  char *rebuilt = NULL;
  TarotXmlParser *parser = tarot_xml_parser_alloc (xml);
  TarotXmlParserIterator *iterator;
  TarotGame *game = tarot_game_alloc ();
  const TarotGameEvent *event;
  if (parser == NULL)
    {
      abort ();
    }
  iterator = tarot_xml_parser_iterator (parser);
  if (iterator == NULL)
    {
      abort ();
    }
  while ((event = tarot_xml_parser_iterator_next_value (iterator)) != NULL)
    {
      if (tarot_game_add_event (game, event) != TAROT_GAME_OK)
	{
	  abort ();
	}
    }
  rebuilt = tarot_game_save_to_xml_alloc (game);
  if (rebuilt == NULL)
    {
      abort ();
    }
  if (strcmp (xml, rebuilt) != 0)
    {
      fprintf (stderr, "%s:%d: %s, size %lu\n", __FILE__, __LINE__, rebuilt, strlen (rebuilt));
      abort ();
    }
  free (rebuilt);
  free (game);
}

static void
push (char **buffer, size_t *available, size_t *used, char c)
{
  if (*available <= *used)
    {
      *available *= 2;
      *buffer = realloc (*buffer, *available);
      if (*buffer == NULL)
	{
	  abort ();
	}
    }
  (*buffer)[(*used)++] = c;
}

static char *
read_all (FILE *file)
{
  char *buffer = NULL;
  size_t available = 1;
  size_t used = 0;
  int c;
  buffer = malloc (1);
  if (buffer == NULL)
    {
      abort ();
    }
  while ((c = fgetc (file)) != EOF)
    {
      push (&buffer, &available, &used, c);
    }
  push (&buffer, &available, &used, '\0');
  return buffer;
}

int
main ()
{
  const char *filename = ABS_TOP_SRCDIR "/src/libtarot/xml/check_xml_1.xml";
  FILE *file = fopen (filename, "r");
  char *buffer = NULL;
  if (file == NULL)
    {
      fprintf (stderr, "'%s' is not present\n", filename);
      abort ();
    }
  buffer = read_all (file);
  fprintf (stderr, "%s:%d: Input: %lu bytes\n", __FILE__, __LINE__, strlen (buffer));
  fclose (file);
  unit_test (buffer);
  free (buffer);
  return 0;
}
